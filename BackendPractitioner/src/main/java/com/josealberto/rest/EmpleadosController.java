package com.josealberto.rest;

import com.josealberto.rest.empleados.Capacitacion;
import com.josealberto.rest.empleados.Empleado;
import com.josealberto.rest.empleados.Empleados;
import com.josealberto.rest.repositorios.EmpleadoDAO;
import com.josealberto.rest.utils.Configuracion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path ="/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadoDAO empleadoDAO;

    /**
     *  Obtener empleado por id mediante requestParam
     * **/
    @GetMapping("/empleado")
    public Empleado getEmpleadoById(@RequestParam(value = "id", required = true) int id){
        return empleadoDAO.getEmpleado(id);
    }

    /**
     * Obtener todos los empleados
     * produces = MediaType.APPLICATION_JSON_VALUE
     * **/
    @GetMapping(value = "/empleadoAll")
    public Empleados getAllEmpleados(){
        return empleadoDAO.getAllEmpleados();
    }
    /**
     * Obtener empleado por id mediante pathVariable
     * **/
    /*
    @GetMapping(path = "/{id}")
    public Empleado getEmpleado(@PathVariable("id") int id){
           return empleadoDAO.getEmpleado(id);
    }
*/  @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable("id") int id){
        Empleado emp = empleadoDAO.getEmpleado(id);
        if(emp ==null){
            return ResponseEntity.notFound().build();
        }else{
           return ResponseEntity.ok().body(emp);
        }
    }
    /**
     * Agregar nuevo empleado
     * **/
    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado empleado){
        Integer id  = empleadoDAO.getAllEmpleados().getListaEmpleados().size()+1;
        empleado.setId(id);
        empleadoDAO.addEmpleado(empleado);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
                buildAndExpand(empleado.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * Actualizar empleado por RequestBody
     * **/
    @PutMapping(path = "/",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateEmpleado(@RequestBody Empleado empleado){
        empleadoDAO.updateEmpleado(empleado);
        return ResponseEntity.ok().build();
    }

    /**
     * Actualizar empleado pasando id por PatVariable
     * **/
    @PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateEmpleado(@PathVariable int id, @RequestBody Empleado empleado){
        empleadoDAO.updateEmpleado(empleado);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleadoById(@PathVariable int id){
        String resp = empleadoDAO.deleteEmpleado(id);
        if (resp == "OK"){
            return  ResponseEntity.ok().build();
        }else{
            return  ResponseEntity.noContent().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> softUpdateEmpleado(@PathVariable int id, @RequestBody Map<String,Object> updates){
        empleadoDAO.sofUpdate(id, updates);
        return  ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id){
            return ResponseEntity.ok().body(empleadoDAO.getCapacitacionesEmpleado(id));
    }

    @PostMapping(path = "/{id}/capacitaciones",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addCapacitacion(@PathVariable int id, @RequestBody Capacitacion capacitacion){
        if(empleadoDAO.addCapacitacion(id,capacitacion)){
            return  ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @Value("${app.autor}")
    private String autor;
    @GetMapping("/autor")
    public String getTitulo() {
        return autor;
    }

    @Autowired
    private Environment env;
    @GetMapping("/autorByEnv")
    public String getTituloByEnv(){
        return env.getProperty("app.autor");
    }

    @Autowired
    private Configuracion configuracion;

    @GetMapping("/autorByConfigClass")
    public String getTituloByConfigClass(){
        return configuracion.getAutor();
    }

    @GetMapping("/modo")
    public String getModo(){
        String modo = configuracion.getModo();
        return String.format("%S (%S)", autor, modo);
    }
}
