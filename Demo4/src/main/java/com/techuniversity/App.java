package com.techuniversity;



import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
        String resp ="";
        try {
            //Users.cargarUsers();
            while (!resp.equals("S")){
                System.out.println("Ingresa el número de id. (S) salir");
                resp = entrada.readLine();
                JSONObject user = Users.getUser(resp);
                mostrarUser(user);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());

        }
    }
    public static void mostrarUser(JSONObject user) throws Exception {
        if(user == null){
            System.out.println("Usuario no encontrado");
        }
        else {
            System.out.println("UserId "+ user.getString("userId"));
            System.out.println("firstName "+ user.getString("firstName"));
            System.out.println("lastName "+ user.getString("lastName"));
            System.out.println("phoneNumber "+ user.getString("phoneNumber"));
            System.out.println("emailAddress "+ user.getString("emailAddress"));
        }
    }

}
