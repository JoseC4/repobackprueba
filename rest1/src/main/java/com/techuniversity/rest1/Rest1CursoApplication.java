package com.techuniversity.rest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rest1CursoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Rest1CursoApplication.class, args);
	}

}
