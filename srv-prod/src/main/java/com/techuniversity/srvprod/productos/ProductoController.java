package com.techuniversity.srvprod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v1")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return  productoService.buscarTodos();
    }

    @PostMapping("/agregarProducto")
    public ProductoModel posProducto(@RequestBody ProductoModel prod){
        return productoService.save(prod);
    }

    @PostMapping("/actualizar")
    public void putProductos(@RequestBody ProductoModel prod){
        productoService.save(prod);
    }

    @DeleteMapping("/borrar")
    public boolean deleteProducto(@RequestBody ProductoModel prod){
        return  productoService.delete(prod);
    }


}
